package br.ucsal.testequalidade20191.locadora;

import br.ucsal.testequalidade20191.locadora.dominio.Modelo;
import br.ucsal.testequalidade20191.locadora.dominio.Veiculo;
import br.ucsal.testequalidade20191.locadora.dominio.enums.SituacaoVeiculoEnum;

public class VeiculoBuilder {
	

	public static final String PLACA_DEFAULT = "";
	public static final Integer ANO_DEFAULT = 2001;
	private static final Modelo MODELO_DEFAULT = null;
	private static final Double VALOR_DEFAULT = 200d;
	private static final SituacaoVeiculoEnum SITUACAO_DEFAULT = SituacaoVeiculoEnum.DISPONIVEL;
	
	private String placa;
	private Integer ano;
	private Modelo modelo;
	private Double valorDiaria;
	private SituacaoVeiculoEnum situacao;
	
	private VeiculoBuilder() {
		// TODO Auto-generated constructor stub
	}
	
	public static VeiculoBuilder umVeiculo() {
		return new VeiculoBuilder();
	}
	
	public VeiculoBuilder comPlaca(String placa) {
		this.placa = placa;
		return this;
	} 
	
	public VeiculoBuilder comAno(Integer ano) {
		this.ano = ano;
		return this;
	}
	
	public VeiculoBuilder comModelo(Modelo modelo) {
		this.modelo = modelo;
		return this;
	}
	
	public VeiculoBuilder comValorDiaria(Double valorDiaria) {
		this.valorDiaria = valorDiaria;
		return this;
	}
	
	public VeiculoBuilder comSituacao(SituacaoVeiculoEnum situacao) {
		this.situacao = situacao;
		return this;
	}
	
	public VeiculoBuilder comSituacaoDisponivel() {
		this.situacao = SituacaoVeiculoEnum.DISPONIVEL;
		return this;
	}
	
	public VeiculoBuilder comSituacaoLocada() {
		this.situacao = SituacaoVeiculoEnum.LOCADO;
		return this;
	}
	
	public VeiculoBuilder comSituacaoManutencao() {
		this.situacao = SituacaoVeiculoEnum.MANUTENCAO;
		return this;
	}
	
	public VeiculoBuilder mas() {
		return VeiculoBuilder
				.umVeiculo()
				.comPlaca(placa)
				.comAno(ano)
				.comModelo(modelo)
				.comValorDiaria(valorDiaria)
				.comSituacao(situacao);
				
	}
	
	public Veiculo build() {
		return new Veiculo(placa, ano, modelo, valorDiaria);
	}
	
}
