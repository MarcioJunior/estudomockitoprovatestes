package br.ucsal.testequalidade20191.locadora;

import static org.mockito.ArgumentMatchers.any;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import br.ucsal.testequalidade20191.locadora.business.LocacaoBO;
import br.ucsal.testequalidade20191.locadora.dominio.Cliente;
import br.ucsal.testequalidade20191.locadora.dominio.Locacao;
import br.ucsal.testequalidade20191.locadora.dominio.Modelo;
import br.ucsal.testequalidade20191.locadora.dominio.Veiculo;
import br.ucsal.testequalidade20191.locadora.persistence.ClienteDAO;
import br.ucsal.testequalidade20191.locadora.persistence.LocacaoDAO;
import br.ucsal.testequalidade20191.locadora.persistence.VeiculoDAO;

@RunWith(MockitoJUnitRunner.class)
public class LocacaoBOUnitarioTest {

	/**
	 * Locar, para um cliente cadastrado, um veículo disponível. 
	 * Método: public static void locarVeiculos(String cpfCliente, List<String> placas, Date
	 * dataLocacao, Integer quantidadeDiasLocacao) throws
	 * ClienteNaoEncontradoException, VeiculoNaoEncontradoException,
	 * VeiculoNaoDisponivelException, CampoObrigatorioNaoInformado {
	 *
	 * Observação: lembre-se de mocar os métodos necessários nas classes
	 * ClienteDAO, VeiculoDAO e LocacaoDAO.
	 * 
	 * @throws Exception
	 */
	
	@Mock
	VeiculoDAO veiculoDAO;
	
	@Mock
	LocacaoDAO locacaoDAO;
	
	@Mock
	ClienteDAO clienteDAO;
	
	@InjectMocks
	LocacaoBO locacaoBO;

	@Test
	public void locarClienteCadastradoUmVeiculoDisponivel() throws Exception {
		String placa = "Myth1234";
		String cpfCliente = "863";
		List<String> placas = new ArrayList<>();
		placas.add(placa);
		Date dataLocacao = new Date();
		Integer quantidadeDiasLocacao = 2;
		
		Cliente cliente = new Cliente(cpfCliente, "Marcio", "123456789");
//		Veiculo veiculo = VeiculoBuilder
//							.umVeiculo()
//							.comPlaca(placa)
//							.comAno(2010)
//							.comModelo(new Modelo("Sentra"))
//							.comValorDiaria(50d)
//							.comSituacaoDisponivel()
//							.build();
		Veiculo veiculo = new Veiculo(placa, 2010, new Modelo("Veloster"), 800.0);
		List<Veiculo> veiculos = new ArrayList<>();
		veiculos.add(veiculo);
		
		Mockito.when(clienteDAO.obterPorCpf(cpfCliente)).thenReturn(cliente);
		Mockito.when(veiculoDAO.obterPorPlaca(placa)).thenReturn(veiculo);
		
		locacaoBO.locarVeiculos(cpfCliente, placas, dataLocacao, quantidadeDiasLocacao);
		
		Mockito.verify(locacaoDAO).insert((Locacao) any(Object.class));
	}

	}

